import { first } from "cypress/types/lodash"

describe('Books', () => {
    before(() => {
        
        // cy.visit('/users/new')
        // cy.get('[id="naam"]').first().type('Anastasia Hellemons')
        // cy.get('[id="user"]').first().type('amchelle')
        // cy.get('[id="pass"]').first().type('n2122681')
        // cy.get('[id="email"]').first().type('amchelle@avans.nl')
        // cy.get('[id="reg"]').first().click()
        

    })

    beforeEach(() => {
        cy.visit('/login')
        cy.get('[id="log-user"]').first().type('amchelle')
        cy.get('[id="log-pass"]').first().type('n2122681')
        cy.get('[id="log"]').first().click()
        cy.visit('/books')
    })

    it('can add book', () => {
        cy.get('[id="title"]').last().should('not.have.text', "the Circle")
        cy.get('[id="new"]').first().click()

        cy.get('[id="titel"]').first().type('the Circle')
        cy.get('[id="auth"]').first().type('David Eggers')
        cy.get('[id="isbn"]').first().type('0345807294')
        cy.get('[id="date"]').first().type('2014-04-22')
        cy.get('[id="page"]').first().type('497')
        cy.get('[id="genre-select"]').eq(2)
        cy.get('[id="sub"]').first().click()

        cy.get('[id="title"]').last().should('have.text', "the Circle")

    })

    it('can remove book', () => {
        cy.get('[id="title"]').last().should('have.text', "the Circle")

        cy.get('[id="info"]').last().click()
        cy.get('[id="del"]').first().click()

        cy.get('[id="title"]').last().should('not.have.text', "the Circle")
    })

    after(() => {
        cy.get('[id="logout"]').first().click()
    })
})