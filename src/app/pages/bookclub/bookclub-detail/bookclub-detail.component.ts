import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { isJSDocThisTag } from 'typescript'
import { AuthService } from '../../auth/auth.service'
import { Book } from '../../book/book.model'
import { Readalong } from '../../readalong/readalong.model'
import { User } from '../../user/user.model'
import { Bookclub } from '../bookclub.model'
import { BookclubService } from '../bookclub.service'

@Component({
  selector: 'app-bookclub-detail',
  templateUrl: './bookclub-detail.component.html',
  styles: []
})
export class BookclubDetailComponent implements OnInit {
  bookclubId: string = ''
  bookclub!: Bookclub
  readalongs: Array<Readalong> = []
  books: Array<Book> = []
  members: Array<User> = []
  userId: string = ''

  constructor(
    private route: ActivatedRoute,
    private bookclubService: BookclubService,
    public authService: AuthService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((parameters) => {
      this.bookclubService.getUserList().subscribe(() => {
        this.bookclubService.getBookList().subscribe(() => {
          this.bookclubId = parameters.get('id') || ''
          this.bookclubService.getBookclubById(this.bookclubId).subscribe((bookclub: Bookclub) => {
            this.bookclub = bookclub
            console.log("bookclub: ", this.bookclub)
            this.userId = bookclub.userId

            this.bookclubService.getReadalongList().subscribe(() => {
              this.bookclubService.getReadalongs(this.bookclubId).subscribe((readalong) => {
                readalong.forEach((readalong) => {
                  this.readalongs.push(readalong)
                })
              })
            })

            this.bookclubService.getBooks(this.bookclubId).subscribe((bookc) => {bookc.forEach((book) => {
              this.books.push(book)})
              console.log("books",this.books)
            })

            this.bookclubService.getUsers(this.bookclubId).subscribe((memberz) => {
              memberz.forEach((member) => {
                this.members.push(member)
              })
            })
          })
        })
      })
    })
  }

  removeBookclub(id: string) {
    this.bookclubService.removeBookclub(id).subscribe(() => {
      this.router.navigate(['..'], { relativeTo: this.route })
    })
  }

  registerMember() {
    if (!this.isRegisterd()) {
      this.bookclubService.registerMember(this.bookclubId).subscribe(() => {
        this.router.navigate(['register'], { relativeTo: this.route })
      })
    }
  }

  unregisterMember() {
    this.bookclubService.unregisterMember(this.bookclubId).subscribe(() => {
      this.router.navigate(['unregister'], { relativeTo: this.route })
    })
  }

  public registerReadalong() {
    this.router.navigate(['registerreadalong'], { relativeTo: this.route })
  }

  unregisterReadalong(id: string) {
    this.bookclubService.unregisterReadalong(this.bookclubId, id).subscribe(() => {
      this.router.navigate(['unregisterreadalong'], {relativeTo: this.route})
    })
  }

  unregisterBook(id: string) {
    this.bookclubService.unregisterBook(this.bookclubId, id).subscribe(() => {
      this.router.navigate(['unregisterbook'], {relativeTo: this.route})
    })
  }

  public registerBook() {
    this.router.navigate(['registerbook'], { relativeTo: this.route })
  }

  isRegisterd(): boolean {
    var isRegisterd = false

    this.members.forEach((memb) => {
      if (memb.user_id == this.authService.currentUserValue.user_id) isRegisterd = true
    })

    if (this.userId == this.authService.currentUserValue.user_id) isRegisterd = true
    return isRegisterd
  }

  isUserRegisterd(id: string): boolean {
    var isRegisterd = false

    if (id == this.authService.currentUserValue.user_id) isRegisterd = this.isRegisterd()

    return isRegisterd
  }
}
