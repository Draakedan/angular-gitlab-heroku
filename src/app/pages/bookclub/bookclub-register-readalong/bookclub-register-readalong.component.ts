import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { Book } from '../../book/book.model'
import { BookService } from '../../book/book.service'
import { Readalong } from '../../readalong/readalong.model'
import { ReadalongService } from '../../readalong/readalong.service'
import { BookclubService } from '../bookclub.service'

@Component({
  selector: 'app-bookclub-register-readalong',
  templateUrl: './bookclub-register-readalong.component.html',
  styleUrls: ['./bookclub-register-readalong.component.css']
})
export class BookclubRegisterReadalongComponent implements OnInit {
  readalongs: Array<Readalong> = []
  readalongId: string = ''
  bookclubId: string = ''

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private bookclubService: BookclubService,
    private readalongService: ReadalongService
  ) {}

  ngOnInit(): void {
    this.readalongService.getReadalongs().subscribe((books) => (this.readalongs = books))
    this.route.paramMap.subscribe((parameters) => {
      this.bookclubId = parameters.get('id') || ''
    })
  }

  onSubmit(): void {
    this.bookclubService.registerReadalong(this.bookclubId, this.readalongId).subscribe(() => {
      this.router.navigate(['..'], { relativeTo: this.route })
    })
  }
}
