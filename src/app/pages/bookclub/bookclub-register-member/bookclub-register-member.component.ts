import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'

@Component({
  selector: 'app-bookclub-register-member',
  templateUrl: './bookclub-register-member.component.html',
  styleUrls: ['./bookclub-register-member.component.css']
})
export class BookclubRegisterMemberComponent implements OnInit {
  constructor(private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {}

  return() {
    this.router.navigate(['..'], { relativeTo: this.route })
  }
}
