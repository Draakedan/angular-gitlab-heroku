import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-bookclub-unregister-readalong',
  templateUrl: './bookclub-unregister-readalong.component.html',
  styleUrls: ['./bookclub-unregister-readalong.component.css']
})
export class BookclubUnregisterReadalongComponent implements OnInit {
  constructor(private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {}

  public return() {
    this.router.navigate(['..'], { relativeTo: this.route })
  }

}
