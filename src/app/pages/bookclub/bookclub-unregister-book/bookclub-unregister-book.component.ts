import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-bookclub-unregister-book',
  templateUrl: './bookclub-unregister-book.component.html',
  styleUrls: ['./bookclub-unregister-book.component.css']
})
export class BookclubUnregisterBookComponent implements OnInit {
  constructor(private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {}

  return() {
    this.router.navigate(['..'], { relativeTo: this.route })
  }
}
