import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'

@Component({
  selector: 'app-bookclub-unregister-member',
  templateUrl: './bookclub-unregister-member.component.html',
  styleUrls: ['./bookclub-unregister-member.component.css']
})
export class BookclubUnregisterMemberComponent implements OnInit {
  constructor(private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {}

  return() {
    this.router.navigate(['..'], { relativeTo: this.route })
  }
}
