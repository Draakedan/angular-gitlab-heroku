import { THIS_EXPR } from '@angular/compiler/src/output/output_ast'
import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, ParamMap, Router } from '@angular/router'
import { of, switchMap, tap } from 'rxjs'
import { Book, Genres } from '../../book/book.model'
import { BookService } from '../../book/book.service'
import { Readalong } from '../../readalong/readalong.model'
import { User } from '../../user/user.model'
import { Bookclub } from '../bookclub.model'
import { BookclubService } from '../bookclub.service'

@Component({
  selector: 'app-bookclub-edit',
  templateUrl: './bookclub-edit.component.html',
  styles: []
})
export class BookclubEditComponent implements OnInit {
  bookclubId: string | null = null
  bookclub: Bookclub = new Bookclub()
  books: Array<Book> = []
  subscription: any
  genreEnumKeys: string[] = []

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private bookclubService: BookclubService,
    private bookService: BookService
  ) {
    this.genreEnumKeys = Object.keys(Genres)
  }

  ngOnInit(): void {
    this.bookService.getBooks().subscribe((books) => (this.books = books))
    this.subscription = this.route.paramMap
      .pipe(
        tap(console.log),
        switchMap((params: ParamMap) => {
          if (!params.get('id')) {
            return of({
              naam: '',
              _id: '',
              hoofd: new User(),
              favorieteBoek: new Book(),
              favBoekId: '',
              hoofdGenre: Genres.fantasy,
              readalongs: [],
              aanTeRadenBoeken: [],
              leden: [],
              userId: ''
            })
          } else {
            return this.bookclubService.getBookclubById(params.get('id') || '')
          }
        })
      )
      .subscribe((bookclub) => {
        this.bookclub = bookclub
      })
  }

  onSubmit(): void {
    this.bookclub.favorieteBoek = this.bookService.getOneBook(this.bookclub.favBoekId)
    if (this.bookclub._id) {
      this.bookclubService.editBookclub(this.bookclub).subscribe(() => {
        this.router.navigate(['..'], { relativeTo: this.route })
      })
    } else {
      this.bookclubService.addBookclub(this.bookclub).subscribe(() => {
        this.router.navigate(['..'], { relativeTo: this.route })
      })
    }
  }
}
