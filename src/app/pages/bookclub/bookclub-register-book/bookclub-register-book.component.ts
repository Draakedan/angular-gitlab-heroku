import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { Book } from '../../book/book.model'
import { BookService } from '../../book/book.service'
import { BookclubService } from '../bookclub.service'

@Component({
  selector: 'app-bookclub-register-book',
  templateUrl: './bookclub-register-book.component.html',
  styleUrls: ['./bookclub-register-book.component.css']
})
export class BookclubRegisterBookComponent implements OnInit {
  books: Array<Book> = []
  bookId: string = ''
  bookclubId: string = ''

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private bookclubService: BookclubService,
    private bookService: BookService
  ) {}

  ngOnInit(): void {
    this.bookService.getBooks().subscribe((books) => (this.books = books))
    this.route.paramMap.subscribe((parameters) => {
      this.bookclubId = parameters.get('id') || ''
    })
  }

  onSubmit(): void {
    console.log(this.bookId)
    this.bookclubService.registerBook(this.bookclubId, this.bookId).subscribe(() => {
      this.router.navigate(['..'], { relativeTo: this.route })
    })
  }
}
