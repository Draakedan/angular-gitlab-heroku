import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { AuthService } from '../../auth/auth.service'
import { Bookclub } from '../bookclub.model'
import { BookclubService } from '../bookclub.service'

@Component({
  selector: 'app-bookclub-list',
  templateUrl: './bookclub-list.component.html',
  styles: []
})
export class BookclubListComponent implements OnInit {
  bookclubs: Bookclub[] = []

  constructor(
    private bookclubService: BookclubService,
    public authService: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.bookclubService.getUserList().subscribe(() => {
      this.bookclubService.getBookList().subscribe(() => {
        this.bookclubService.getBookclubs().subscribe((bookclubs) => {
          this.bookclubs = bookclubs
          console.log(bookclubs)
        })
      })
    })
  }
}
