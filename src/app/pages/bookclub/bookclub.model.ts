import { Book, Genres } from '../book/book.model'
import { Readalong } from '../readalong/readalong.model'
import { User } from '../user/user.model'

export class Bookclub {
  naam: string = ''
  _id: string = ''
  hoofd: User = new User()
  favorieteBoek: Book = new Book()
  favBoekId: string = ''
  hoofdGenre: Genres = Genres.fantasy
  readalongs: Array<Readalong> = [new Readalong()]
  aanTeRadenBoeken: Array<Book> = [new Book()]
  leden: Array<User> = [new User()]
  userId: string = ''
}
