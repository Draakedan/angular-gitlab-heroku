import { Injectable } from '@angular/core'
import { textChangeRangeIsUnchanged } from 'typescript'
import { Book } from '../book/book.model'
import { BookService } from '../book/book.service'
import { Readalong } from '../readalong/readalong.model'
import { ReadalongService } from '../readalong/readalong.service'
import { User } from '../user/user.model'
import { UserService } from '../user/user.service'
import { Bookclub } from './bookclub.model'
import { Genres } from '../book/book.model'
import { AuthService } from '../auth/auth.service'
import { HttpClient } from '@angular/common/http'
import { catchError, map, Observable, of } from 'rxjs'
import { environment } from 'src/environments/environment'

@Injectable({
  providedIn: 'root'
})
export class BookclubService {
  constructor(
    private bookService: BookService,
    private userService: UserService,
    private readalongService: ReadalongService,
    private http: HttpClient,
    private authService: AuthService
  ) {}

  bookclubs: Array<Bookclub> = []

  getReadalongList(): Observable<any> {
    return this.readalongService.getReadalongs()
  }

  getBookList(): Observable<any> {
    return this.bookService.getBooks()
  }

  getUserList(): Observable<any> {
    return this.userService.getUsers()
  }

  getBookclubs(): Observable<Array<Bookclub>> {
    this.bookclubs = []
    return this.http.get(`${environment.apiUrl}bookclubs`, { headers: this.authService.getToken() }).pipe(
      map((response: any) => {
        response.forEach((bookclub: Bookclub) => {
          bookclub.favorieteBoek = this.bookService.getOneBook(bookclub.favBoekId)
          bookclub.hoofd = this.userService.getOneUser(bookclub.userId)
          this.bookclubs.push(bookclub)
        })
        return this.bookclubs
      }),
      catchError((error: any) => {
        console.log('error:', error)
        console.log('error.message:', error.message)
        console.log('error.error.message:', error.error.message)
        return undefined!
      })
    )
  }

  getBookclubById(id: string): Observable<Bookclub> {
    return this.http
      .get(`${environment.apiUrl}bookclubs/${id}`, { headers: this.authService.getToken() })
      .pipe(
        map((response: any) => {
          let bookclub = { ...response } as Bookclub
          bookclub.favorieteBoek = this.bookService.getOneBook(bookclub.favBoekId)
          bookclub.hoofd = this.userService.getOneUser(bookclub.userId[0])
          return bookclub
        }),
        catchError((error: any) => {
          console.log('error:', error)
          console.log('error.message:', error.message)
          console.log('error.error.message:', error.error.message)
          return undefined!
        })
      )
  }

  addBookclub(book: Bookclub): Observable<any> {
    return this.http
      .post(
        `${environment.apiUrl}bookclubs`,
        { ...book, id: this.authService.currentUserValue.user_id },
        { headers: this.authService.getToken() }
      )
      .pipe(
        map((response: any) => {
          let book = { ...response } as Bookclub
          return book
        }),
        catchError((error: any) => {
          console.log('error:', error)
          console.log('error.message:', error.message)
          console.log('error.error.message:', error.error.message)
          return undefined!
        })
      )
  }

  editBookclub(book: Bookclub): Observable<any> {
    return this.http
      .put(
        `${environment.apiUrl}bookclubs/${book._id}`,
        { ...book },
        { headers: this.authService.getToken() }
      )
      .pipe(
        map((response: any) => {
          let book = { ...response } as Bookclub
          return book
        }),
        catchError((error: any) => {
          console.log('error:', error)
          console.log('error.message:', error.message)
          console.log('error.error.message:', error.error.message)
          return undefined!
        })
      )
  }

  removeBookclub(id: string): Observable<any> {
    return this.http
      .post(
        `${environment.apiUrl}bookclubs/${id}`,
        { id: this.getOneBookclub(id).userId[0] },
        { headers: this.authService.getToken() }
      )
      .pipe(
        map((response: any) => {
          let book = { ...response } as Bookclub
          this.getBookclubs()
          return book
        }),
        catchError((error: any) => {
          console.log('error:', error)
          console.log('error.message:', error.message)
          console.log('error.error.message:', error.error.message)
          return undefined!
        })
      )
  }

  getReadalongs(id: string): Observable<Array<Readalong>> {
    let readalongs: Array<Readalong> = []
    return this.http
      .get(`${environment.apiUrl}bookclubs/${id}/readalongs`, { headers: this.authService.getToken() })
      .pipe(
        map((response: any) => {
          this.readalongService.getAllReadalongsOfList(response).forEach((item) => {
            readalongs.push(item)
            console.log('item', item)
            return readalongs
          })
          console.log(readalongs)
          return readalongs
        }),
        catchError((error: any) => {
          console.log('error:', error)
          console.log('error.message:', error.message)
          console.log('error.error.message:', error.error.message)
          return undefined!
        })
      )
  }

  getBooks(id: string): Observable<Array<Book>> {
    let books: Array<Book> = []
    return this.http
      .get(`${environment.apiUrl}bookclubs/${id}/books`, { headers: this.authService.getToken() })
      .pipe(
        map((response: any) => {
          this.bookService.getBooksFromList(response).forEach((item) => {
            books.push(item)
            return books
          })
          return books
        }),
        catchError((error: any) => {
          console.log('error:', error)
          console.log('error.message:', error.message)
          console.log('error.error.message:', error.error.message)
          return undefined!
        })
      )
  }

  getUsers(id: string): Observable<Array<User>> {
    console.log('getting members')
    let users: Array<User> = []
    return this.http
      .get(`${environment.apiUrl}bookclubs/${id}/members`, { headers: this.authService.getToken() })
      .pipe(
        map((response: any) => {
          console.log('res', response)
          let list = { ...response } as Array<Array<User>>
          console.log(list[0])
          users = list[0]
          return users
        }),
        catchError((error: any) => {
          console.log('error:', error)
          console.log('error.message:', error.message)
          console.log('error.error.message:', error.error.message)
          return undefined!
        })
      )
  }

  registerMember(id: string): Observable<any> {
    console.log(id)
    return this.http
      .post(
        `${environment.apiUrl}bookclubs/${id}/members/register`,
        { id: this.authService.currentUserValue.user_id },
        { headers: this.authService.getToken() }
      )
      .pipe(
        map((response: any) => {
          console.log(response)
          return response
        }),
        catchError((error: any) => {
          console.log('error:', error)
          console.log('error.message:', error.message)
          console.log('error.error.message:', error.error.message)
          return undefined!
        })
      )
  }

  unregisterMember(id: string): Observable<any> {
    console.log(id)
    return this.http
      .put(
        `${environment.apiUrl}bookclubs/${id}/members/unregister`,
        { id: this.authService.currentUserValue.user_id },
        { headers: this.authService.getToken() }
      )
      .pipe(
        map((response: any) => {
          console.log(response)
          return response
        }),
        catchError((error: any) => {
          console.log('error:', error)
          console.log('error.message:', error.message)
          console.log('error.error.message:', error.error.message)
          return undefined!
        })
      )
  }

  registerBook(bookclubId: string, bookId: string): Observable<any> {
    return this.http
      .post(
        `${environment.apiUrl}bookclubs/${bookclubId}/books/register`,
        { bookId: bookId },
        { headers: this.authService.getToken() }
      )
      .pipe(
        map((response: any) => {
          console.log(response)
          return response
        }),
        catchError((error: any) => {
          console.log('error:', error)
          console.log('error.message:', error.message)
          console.log('error.error.message:', error.error.message)
          return undefined!
        })
      )
  }

  unregisterBook(bookclubId: string, bookId: string): Observable<any> {
    return this.http
      .put(
        `${environment.apiUrl}bookclubs/${bookclubId}/books/unregister`,
        { bookId: bookId },
        { headers: this.authService.getToken() }
      )
      .pipe(
        map((response: any) => {
          console.log(response)
          return response
        }),
        catchError((error: any) => {
          console.log('error:', error)
          console.log('error.message:', error.message)
          console.log('error.error.message:', error.error.message)
          return undefined!
        })
      )
  }

  registerReadalong(bookclubId: string, readalongId: string): Observable<any> {
    return this.http
      .post(
        `${environment.apiUrl}bookclubs/${bookclubId}/readalongs/register`,
        { readalongId: readalongId },
        { headers: this.authService.getToken() }
      )
      .pipe(
        map((response: any) => {
          console.log(response)
          return response
        }),
        catchError((error: any) => {
          console.log('error:', error)
          console.log('error.message:', error.message)
          console.log('error.error.message:', error.error.message)
          return undefined!
        })
      )
  }

  unregisterReadalong(bookclubId: string, readalongId: string): Observable<any> {
    return this.http
      .put(
        `${environment.apiUrl}bookclubs/${bookclubId}/readalongs/unregister`,
        { readalongId: readalongId },
        { headers: this.authService.getToken() }
      )
      .pipe(
        map((response: any) => {
          console.log(response)
          return response
        }),
        catchError((error: any) => {
          console.log('error:', error)
          console.log('error.message:', error.message)
          console.log('error.error.message:', error.error.message)
          return undefined!
        })
      )
  }

  getOneBookclub(id: string): Bookclub {
    return this.bookclubs.filter((booc) => booc._id === id)[0]
  }
}
