import { Component, OnInit } from '@angular/core'
import { UseCase } from './usecases/usecase.model'

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html'
})
export class AboutComponent implements OnInit {
  readonly PLAIN_USER = 'Reguliere gebruiker'
  readonly ADMIN_USER = 'Administrator'

  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Inloggen',
      description: 'Hiermee logt een bestaande gebruiker in.',
      scenario: [
        'Gebruiker vult email en password in en klikt op Login knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.'
      ],
      actor: this.PLAIN_USER,
      precondition: 'Geen',
      postcondition: 'De actor is ingelogd'
    },
    {
      id: 'UC-02',
      name: 'Toevoegen boek',
      description: 'Hiermee kan een administrator een nieuwe boek toevogen.',
      scenario: [
        'Administrator klikt op de nieuwe boek knop.',
              'Het edit boek form wordt geopend met lege velden.',
               'Administrator voert de gegenvens in van een nieuwe boek.',
               'Indien velden valide zijn dan wordt het boek toegevoed en redirect de applicatie naar de boeken lijst scherm.'
              ],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd en bevind zich op de lijst scherm van een boek',
      postcondition: 'Het boek is aangemaakt'
    },
    {
      id: 'UC-03',
      name: 'Toevoegen boek',
      description: 'Hiermee kan een gebruiker een nieuwe boek toevogen.',
      scenario: [
        'Gebruiker klikt op de nieuwe boek knop.',
              'Het edit boek form wordt geopend met lege velden.',
               'Gebruiker voert de gegenvens in van een nieuwe boek.',
               'Indien velden valide zijn dan wordt het boek toegevoed en redirect de applicatie naar de boeken lijst scherm.'
              ],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd en bevind zich op de lijst scherm van een boek',
      postcondition: 'Het boek is aangemaakt'
    },
    {
      id: 'UC-04',
      name: 'Aanpassen van een boek',
      description : 'Hiermee kan een administrator een bestaande boek aanpassen.',
      scenario: [
        'Administrator klikt op de edit boek knop.',
        'het edit boek form wordt geopend met de velden open op de huidige boek data.',
        'Administrator voert de nieuwe waardes in en drukt op de ok knop.',
        'Indien velden valiede zijn dan wordt het boek geupdate en redirect de applicatie naar de detail scherm'
      ],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd en bevind zich op de detail scherm van een boek',
      postcondition: 'De informatie van het boek is aangepast'
    },
    {
      id: 'UC-05',
      name: 'Aanpassen van een boek',
      description : 'Hiermee kan een gebruiker een bestaande boek, die hij zelf heeft aangemaakt, aanpassen.',
      scenario: [
        'Gebruiker klikt op de edit boek knop.',
        'Het edit boek form wordt geopend met de velden open op de huidige boek data.',
        'Gebruiker voert de nieuwe waardes in en drukt op de ok knop.',
        'Indien velden valiede zijn dan wordt het boek geupdate en redirect de applicatie naar de detail scherm'
      ],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd en bevind zich op de detail scherm van een boek die hij zelf heeft aangemaakt',
      postcondition: 'De informatie van het boek is aangepast'
    },
    {
      id: 'UC-06',
      name: 'verwijderen van een boek',
      description : 'Hiermee kan een administrator een bestaande boek verwijderen.',
      scenario: [
        'Administrator klikt op de verwijder boek knop.',
        'Een popup wordt op het scherm getoond om te vragen of de administator echt het boek wil verwijderen',
        'Administrator drukt op de ok knop.',
        'Het boek wordt verwijdered en redirect de applicatie naar de boeken lijst scherm'
      ],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd en bevind zich op de detail scherm van een boek',
      postcondition: 'Het boek is verwijderd'
    },
    {
      id: 'UC-07',
      name: 'verwijderen van een boek',
      description : 'Hiermee kan een gebruiker een bestaande boek, die hij zelf heeft aangemaakt, verwijderen.',
      scenario: [
        'Gebruker klikt op de verwijder boek knop.',
        'Een popup wordt op het scherm getoond om te vragen of de administator echt het boek wil verwijderen',
        'Gebruiker drukt op de ok knop.',
        'Het boek wordt verwijdered en redirect de applicatie naar de boeken lijst scherm'
      ],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd en bevind zich op de detail scherm van een boek die hij zelf heeft aangemaakt',
      postcondition: 'Het boek is verwijderd'
    },
    {
      id: 'UC-08',
      name: 'Toevoegen readalong',
      description: 'Hiermee kan een administrator een nieuwe readalong aanmaken.',
      scenario: [
        'Administrator klikt op de nieuwe readalong knop.',
              'Het edit readalong form wordt geopend met lege velden.',
               'Administrator voert de gegenvens in van een nieuwe readalong.',
               'Indien velden valide zijn dan wordt het readalong aangemaakt en redirect de applicatie naar de readalong lijst scherm.'
              ],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd en bevind zich op de lijst scherm van een readalong',
      postcondition: 'De readalong is aangemaakt'
    },
    {
      id: 'UC-09',
      name: 'Toevoegen readalong',
      description: 'Hiermee kan een gebruiker een nieuwe readalong aanmaken.',
      scenario: [
        'Gebruiker klikt op de nieuwe readalong knop.',
              'Het edit readalong form wordt geopend met lege velden.',
               'Gebruiker voert de gegenvens in van een nieuwe readalong.',
               'Indien velden valide zijn dan wordt de readalong toegevoed en redirect de applicatie naar de readalong lijst scherm.'
              ],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd en bevind zich op de lijst scherm van een readalong',
      postcondition: 'De readalong is aangemaakt'
    },
    {
      id: 'UC-10',
      name: 'Aanpassen van een readalong',
      description : 'Hiermee kan een administrator een bestaande readalong aanpassen.',
      scenario: [
        'Administrator klikt op de edit readalong knop.',
        'het edit readalong form wordt geopend met de velden open op de huidige readalong.',
        'Administrator voert de nieuwe waardes in en drukt op de ok knop.',
        'Indien velden valiede zijn dan wordt de readalong geupdate en redirect de applicatie naar de detail scherm'
      ],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd en bevind zich op de detail scherm van een readalong',
      postcondition: 'De informatie van de readalong is aangepast'
    },
    {
      id: 'UC-11',
      name: 'Aanpassen van een readalong',
      description : 'Hiermee kan een gebruiker een bestaande readalong, die hij zelf heeft aangemaakt, aanpassen.',
      scenario: [
        'Gebruiker klikt op de edit readalong knop.',
        'Het edit readalong form wordt geopend met de velden open op de huidige readalong data.',
        'Gebruiker voert de nieuwe waardes in en drukt op de ok knop.',
        'Indien velden valiede zijn dan wordt de readalong geupdate en redirect de applicatie naar de detail scherm'
      ],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd en bevind zich op de detail scherm van een readalong die hij zelf heeft aangemaakt',
      postcondition: 'De informatie van de readalong is aangepast'
    },
    {
      id: 'UC-12',
      name: 'verwijderen van een readalong',
      description : 'Hiermee kan een administrator een bestaande readalong verwijderen.',
      scenario: [
        'Administrator klikt op de verwijder readalong knop.',
        'Een popup wordt op het scherm getoond om te vragen of de administator echt het readalong wil verwijderen',
        'Administrator drukt op de ok knop.',
        'Het readalong wordt verwijdered en redirect de applicatie naar de readalong lijst scherm'
      ],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd en bevind zich op de detail scherm van een readalong',
      postcondition: 'De readalong is verwijderd'
    },
    {
      id: 'UC-13',
      name: 'verwijderen van een readalong',
      description : 'Hiermee kan een gebruiker een bestaande readalong, die hij zelf heeft aangemaakt, verwijderen.',
      scenario: [
        'Gebruker klikt op de verwijder readalong knop.',
        'Een popup wordt op het scherm getoond om te vragen of de gebruiker echt het readalong wil verwijderen',
        'Gebruiker drukt op de ok knop.',
        'Het readalong wordt verwijdered en redirect de applicatie naar de readalong lijst scherm'
      ],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd en bevind zich op de detail scherm van een readalong die hij zelf heeft aangemaakt',
      postcondition: 'De readalong is verwijderd'
    },
    {
      id: 'UC-14',
      name: 'aanmelden voor een readalong',
      description : 'Hiermee kan een administrator zich bij een bestaande readalong inschrijven',
      scenario: [
        'Administrator klikt op readalong inscrijven knop.',
        'Een popup wordt op het scherm getoond om te vragen of de administrator zich echt will inschrijven voor een readalong',
        'Administrator drukt op de ok knop.',
        'De administrator word ingeschreven en redirect de applicatie naar de readalong lijst scherm'
      ],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd en bevind zich op de detail scherm van een readalong',
      postcondition: 'De actor is aangemeld voor de readalong'
    },
    {
      id: 'UC-15',
      name: 'aanmelden voor een readalong',
      description : 'Hiermee kan een gebruiker zich bij een bestaande readalong inschrijven',
      scenario: [
        'Gebruiker klikt op readalong inscrijven knop.',
        'Een popup wordt op het scherm getoond om te vragen of de gebruiker zich echt will inschrijven voor een readalong',
        'Gebruiker drukt op de ok knop.',
        'De gebruiker word ingeschreven en redirect de applicatie naar de readalong lijst scherm'
      ],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd en bevind zich op de detail scherm van een readalong',
      postcondition: 'De actor is aangemeld voor de readalong'
    },
    {
      id: 'UC-16',
      name: 'Toevoegen boekclub',
      description: 'Hiermee kan een administrator een nieuwe boekclub aanmaken.',
      scenario: [
        'Administrator klikt op de nieuwe boekclub knop.',
              'Het edit boekclub form wordt geopend met lege velden.',
               'Administrator voert de gegenvens in van een nieuwe boekclub.',
               'Indien velden valide zijn dan wordt het boekclub aangemaakt en redirect de applicatie naar de boekclub lijst scherm.'
              ],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd en bevind zich op de lijst scherm van een boekclub',
      postcondition: 'Het boekclub is aangemaakt'
    },
    {
      id: 'UC-17',
      name: 'Toevoegen boekclub',
      description: 'Hiermee kan een gebruiker een nieuwe boekclub aanmaken.',
      scenario: [
        'Gebruiker klikt op de nieuwe boekclub knop.',
              'Het edit boekclub form wordt geopend met lege velden.',
               'Gebruiker voert de gegenvens in van een nieuwe boekclub.',
               'Indien velden valide zijn dan wordt het boekclub aangemaakt en redirect de applicatie naar de boekclub lijst scherm.'
              ],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd en bevind zich op de lijst scherm van een boekclub',
      postcondition: 'Het boekclub is aangemaakt'
    },
    {
      id: 'UC-18',
      name: 'Aanpassen van een boekclub',
      description : 'Hiermee kan een administrator een bestaande boekclub aanpassen.',
      scenario: [
        'Administrator klikt op de edit boekclub knop.',
        'het edit boekclub form wordt geopend met de velden open op de huidige boekclub data.',
        'Administrator voert de nieuwe waardes in en drukt op de ok knop.',
        'Indien velden valiede zijn dan wordt het boekclub geupdate en redirect de applicatie naar de detail scherm'
      ],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd en bevind zich op de detail scherm van een boekclub',
      postcondition: 'De informatie van het boekclub is aangepast'
    },
    {
      id: 'UC-19',
      name: 'Aanpassen van een boekclub',
      description : 'Hiermee kan een gebruiker een bestaande boekclub, die hij zelf heeft aangemaakt, aanpassen.',
      scenario: [
        'Gebruiker klikt op de edit boekclub knop.',
        'Het edit boekclub form wordt geopend met de velden open op de huidige boekclub data.',
        'Gebruiker voert de nieuwe waardes in en drukt op de ok knop.',
        'Indien velden valiede zijn dan wordt het boekclub geupdate en redirect de applicatie naar de detail scherm'
      ],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd en bevind zich op de detail scherm van een boekclub die hij zelf heeft aangemaakt',
      postcondition: 'De informatie van het boekclub is aangepast'
    },
    {
      id: 'UC-20',
      name: 'verwijderen van een boekclub',
      description : 'Hiermee kan een administrator een bestaande boekclub verwijderen.',
      scenario: [
        'Administrator klikt op de verwijder boekclub knop.',
        'Een popup wordt op het scherm getoond om te vragen of de administator echt het boekclub wil verwijderen',
        'Administrator drukt op de ok knop.',
        'Het boekclub wordt verwijdered en redirect de applicatie naar de boekclub lijst scherm'
      ],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd en bevind zich op de detail scherm van een boekclub',
      postcondition: 'Het boekclub is verwijderd'
    },
    {
      id: 'UC-21',
      name: 'verwijderen van een boekclub',
      description : 'Hiermee kan een gebruiker een bestaande boekclub, die hij zelf heeft aangemaakt, verwijderen.',
      scenario: [
        'Gebruker klikt op de verwijder boekclub knop.',
        'Een popup wordt op het scherm getoond om te vragen of de administator echt het boekclub wil verwijderen',
        'Gebruiker drukt op de ok knop.',
        'Het boekclub wordt verwijdered en redirect de applicatie naar de boekclub lijst scherm'
      ],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd en bevind zich op de detail scherm van een boekclub die hij zelf heeft aangemaakt',
      postcondition: 'Het boekclub is verwijderd'
    },
    {
      id: 'UC-22',
      name: 'aanmelden voor een boekclub',
      description : 'Hiermee kan een administrator zich bij een bestaande boekclub inschrijven',
      scenario: [
        'Administrator klikt op boekclub inscrijven knop.',
        'Een popup wordt op het scherm getoond om te vragen of de administrator zich echt will inschrijven voor een boekclub',
        'Administrator drukt op de ok knop.',
        'De administrator word ingeschreven en redirect de applicatie naar de boekclub lijst scherm'
      ],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd en bevind zich op de detail scherm van een boekclub',
      postcondition: 'De actor is aangemeld voor de boekclub'
    },
    {
      id: 'UC-23',
      name: 'aanmelden voor een boekclub',
      description : 'Hiermee kan een gebruiker zich bij een bestaande boekclub inschrijven',
      scenario: [
        'Gebruiker klikt op boekclub inscrijven knop.',
        'Een popup wordt op het scherm getoond om te vragen of de gebruiker zich echt will inschrijven voor een boekclub',
        'Gebruiker drukt op de ok knop.',
        'De gebruiker word ingeschreven en redirect de applicatie naar de boekclub lijst scherm'
      ],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd en bevind zich op de detail scherm van een boekclub',
      postcondition: 'De actor is aangemeld voor de boekclub'
    },
    {
      id: 'UC-24',
      name: 'opgeven van een aangeraden boek aan een boekclub',
      description : 'Hiermee kan een administrator een aante raden boek aan de boekclub aan raden',
      scenario: [
        'Administrator klikt op de raad boek aan knop',
        'Het raad boek aan scherm wordt geopend',
        'Administrator selecteert het boek',
        'Het boek wordt aan de boekclub geraden en redirect de applicatie naar de boekclub lijst scherm'
      ],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd en bevind zich op de detail scherm van een boekclub',
      postcondition: 'Het boek is aan de boekclub geraden'
    },
    {
      id: 'UC-25',
      name: 'opgeven van een aangeraden boek aan een boekclub',
      description : 'Hiermee kan een gebruiker een aante raden boek aan de boekclub aan raden',
      scenario: [
        'Gebruker klikt op de raad boek aan knop',
        'Het raad boek aan scherm wordt geopend',
        'Gebruiker selecteert het boek',
        'Het boek wordt aan de boekclub geraden en redirect de applicatie naar de boekclub lijst scherm'
      ],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd en bevind zich op de detail scherm van een boekclub',
      postcondition: 'Het boek is aan de boekclub geraden'
    },
    {
      id: 'UC-26',
      name: 'opgeven van een readalong aan een boekclub',
      description : 'Hiermee kan een administrator een readalong bij de boekclub registreren',
      scenario: [
        'Administrator klikt op de registreer readalog knop',
        'Het registreer readalong scherm wordt geopend',
        'Administrator selecteert de readalong',
        'De readalong wordt aan de boekclub toegevoed en redirect de applicatie naar de boekclub lijst scherm'
      ],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd en bevind zich op de detail scherm van een boekclub',
      postcondition: 'Het readalong is aan de boekclub toegevoegd'
    },
    {
      id: 'UC-27',
      name: 'toevoegen van een readalong aan een boekclub',
      description : 'Hiermee kan een gebruiker een readalong aan de boekclub toevoegen',
      scenario: [
        'Gebruker klikt op de registreer readalong knop',
        'Het regisreer readalong aan scherm wordt geopend',
        'Gebruiker selecteert de readalong',
        'De readalong wordt aan de boekclub toegevoed en redirect de applicatie naar de boekclub lijst scherm'
      ],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd en bevind zich op de detail scherm van een boekclub',
      postcondition: 'Het readalong is aan de boekclub toegevoegd'
    },
  ]

  constructor() {}

  ngOnInit() {}
}
