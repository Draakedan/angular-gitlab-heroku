import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { catchError, map, Observable } from 'rxjs'
import { observableToBeFn } from 'rxjs/internal/testing/TestScheduler'
import { environment } from 'src/environments/environment'
import { AuthService } from '../auth/auth.service'
import { User, UserRole } from './user.model'

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private users: User[] = []

  constructor(private http: HttpClient, private authService: AuthService) {}

  getUsers(): Observable<Array<User>> {
    this.users = []
    return this.http.get(`${environment.apiUrl}users`, { headers: this.authService.getToken() }).pipe(
      map((response: any) => {
        response.forEach((book: User) => {
          this.users.push(book)
        })
        return this.users;
      }),
      catchError((error: any) => {
        console.log('error:', error)
        console.log('error.message:', error.message)
        console.log('error.error.message:', error.error.message)
        return undefined!
      })
    )
  }

  getUserById(id: string): Observable<User> {
    return this.http.get(`${environment.apiUrl}users/${id}`, { headers: this.authService.getToken() }).pipe(
      map((response: any) => {
        let books = { ...response } as User
        return books
      }),
      catchError((error: any) => {
        console.log('error:', error)
        console.log('error.message:', error.message)
        console.log('error.error.message:', error.error.message)
        return undefined!
      })
    )
  }

  addUser(book: User): Observable<any> {
    this.users.push(book)
    return this.http.post(`${environment.apiUrl}users`, { ...book }).pipe(
      map((response: any) => {
        let book = { ...response } as User
        return book
      }),
      catchError((error: any) => {
        console.log('error:', error)
        console.log('error.message:', error.message)
        console.log('error.error.message:', error.error.message)
        return undefined!
      })
    )
  }

  editUser(book: User): Observable<any> {
    return this.http
      .put(`${environment.apiUrl}users/${book.user_id}`, { ...book }, { headers: this.authService.getToken() })
      .pipe(
        map((response: any) => {
          let book = { ...response } as User
          return book
        }),
        catchError((error: any) => {
          console.log('error:', error)
          console.log('error.message:', error.message)
          console.log('error.error.message:', error.error.message)
          return undefined!
        })
      )
  }

  removeUser(id: string): Observable<any> {
    return this.http.delete(`${environment.apiUrl}users/${id}`, { headers: this.authService.getToken() }).pipe(
      map((response: any) => {
        console.log(response)
        return response
      }),
      catchError((error: any) => {
        console.log('error:', error)
        console.log('error.message:', error.message)
        console.log('error.error.message:', error.error.message)
        return undefined!
      })
    )
  }

  getOneUser(id: string): User {
    return this.users.filter((user) => user.user_id === id)[0]
  }
}
