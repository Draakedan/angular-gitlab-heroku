import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, ParamMap, Router } from '@angular/router'
import { of, switchMap, tap } from 'rxjs'
import { User, UserRole } from '../user.model'
import { UserService } from '../user.service'

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styles: []
})
export class UserEditComponent implements OnInit {
  userId: string | null = null
  user: User
  naam: string | null = null
  subscription: any

  constructor(private route: ActivatedRoute, private router: Router, private userService: UserService) {
    this.user = new User()
  }

  ngOnInit(): void {
    this.subscription = this.route.paramMap
      .pipe(
        tap(console.log),
        switchMap((params: ParamMap) => {
          if (!params.get('id')) {
            return of({
              user_id: '',
              naam: '',
              gebruikersnaam: '',
              wachtwoord: '',
              email: '',
              role: UserRole.generalUser,
              token: ''
            })
          } else {
            return this.userService.getUserById(params.get('id') || '')
          }
        })
      )
      .subscribe((user) => {
        this.user = user
        this.naam = user.naam !== '' ? user.naam : 'nieuw'
      })
  }

  onSubmit(): void {
    if (this.user.user_id) {
      this.userService.editUser(this.user).subscribe(() => {
        this.router.navigate(['..'], { relativeTo: this.route })
      })
    } else {
      this.userService.addUser(this.user).subscribe(() => {
        this.router.navigate(['..'], { relativeTo: this.route })
      })
    }
  }
}
