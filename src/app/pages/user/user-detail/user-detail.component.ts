import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { AuthService } from '../../auth/auth.service'
import { User } from '../user.model'
import { UserService } from '../user.service'

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styles: []
})
export class UserDetailComponent implements OnInit {
  userId: string = ''
  user!: User

  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    public authService: AuthService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((parameters) => {
      this.userId = parameters.get('id') || ''
      this.userService.getUserById(this.userId).subscribe((user) => {
        this.user = user
      })
    })
  }
}
