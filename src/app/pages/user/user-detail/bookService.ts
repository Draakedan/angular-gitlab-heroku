import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { catchError, map, Observable, of, retry } from 'rxjs'
import { environment } from 'src/environments/environment'
import { AuthService } from '../../auth/auth.service'
import { User } from '../../user/user.model'
import { UserService } from '../../user/user.service'
import { Book, Genres } from '../../book/book.model'

@Injectable({
  providedIn: 'root'
})
export class BookService {
  books: Array<Book> = []

  constructor() {}

  getBooks(): Observable<Array<Book>> {
    return of(this.books)
  }

  getBookById(id: string): Observable<Book> {
    return of(this.getOneBook(id))
  }

  addBook(book: Book): Observable<Book> {
    let newBook = book
    newBook._id += book._id
    this.books.push(newBook)
    return of(newBook)
  }

  editBook(book: Book): Observable<any> {
    let index = this.books.indexOf(this.getOneBook(book._id))
    this.books[index] = book
    return of(book)
  }

  removeBook(id: string): Observable<any> {
    let index = this.books.indexOf(this.getOneBook(id))
    this.books.splice(index, 1)
    return of(id)
  }

  getOneBook(id: string): Book {
    let res = this.books.filter((book) => book._id === id)[0]
    return res
  }

  getBooksFromList(ids: Array<string>): Array<Book> {
    let returnBooks: Array<Book> = []
    ids.forEach((id) => {
      returnBooks.push(this.books.filter((book) => book._id === id)[0])
    })
    return returnBooks
  }
}
