export enum UserRole {
  admin = 'admin',
  generalUser = 'generalUser'
}

export class User {
  user_id: string = ''
  naam: string = ''
  gebruikersnaam: string = ''
  wachtwoord: string = ''
  email: string = ''
  role: UserRole = UserRole.generalUser
  token: string = ''

  constructor(naam = '', gebruikersnam = '', email = '') {
    this.naam = naam
    this.gebruikersnaam = gebruikersnam
    this.email = email
  }
}
