import { Component, OnInit } from '@angular/core'
import { User } from '../../user/user.model'
import { ActivatedRoute, Router } from '@angular/router'
import { AuthService } from '../auth.service'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { first } from 'rxjs'
import { AlertService } from '../alert.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: []
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup
  loading = false;
  submitted = false;
  returnUrl: string | undefined;
  

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router, private authService: AuthService, private alertService: AlertService) {
    if (this.authService.currentUserValue)
    this.router.navigate(['/dashboard']);
  }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/dashboard';
  }

  get f() {return this.loginForm.controls;}

  onSubmit() {

    this.submitted = true;
    this.alertService.clear();

    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    this.authService.login(this.f.username.value, this.f.password.value).pipe(first()).subscribe(
      data => {
        this.router.navigate([this.returnUrl]);
      },
      error => {
        this.alertService.error(error);
        this.loading = false;
      }
    );
    
  }
}
