import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { BehaviorSubject, Observable } from 'rxjs'
import { map } from 'rxjs/operators'

import { User, UserRole } from '../user/user.model'
import { userInfo } from 'os'
import { environment } from 'src/environments/environment'
import { Router } from '@angular/router'

@Injectable({ providedIn: 'root' })
export class AuthService {
  private currentUserSubject: BehaviorSubject<User>
  public currentUser: Observable<User>

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')!))
    this.currentUser = this.currentUserSubject.asObservable()
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value
  }

  login(gn: any, ww: any) {
    return this.http.post<any>(`${environment.apiUrl}login`, { gn, ww }).pipe(
      map((user) => {
        localStorage.setItem('currentUser', JSON.stringify(user))
        this.currentUserSubject.next(user)
        return user
      })
    )
  }

  logout() {
    localStorage.removeItem('currentUser')
    this.currentUserSubject.next(null!)
  }

  getToken(): HttpHeaders {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.currentUserValue.token
    })
  }

  userMayEdit(id: string): boolean {
    var mayEdit = this.currentUserValue.user_id == id
    if (this.currentUserValue.role == UserRole.admin) mayEdit = true
    return mayEdit
  }

  adminMayEdit(): boolean {
    return this.currentUserValue.role == UserRole.admin
  }
}
