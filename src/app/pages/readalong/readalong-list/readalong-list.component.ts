import { Component, OnInit } from '@angular/core'
import { AuthService } from '../../auth/auth.service'
import { Readalong } from '../readalong.model'
import { ReadalongService } from '../readalong.service'

@Component({
  selector: 'app-readalong-list',
  templateUrl: './readalong-list.component.html',
  styles: []
})
export class ReadalongListComponent implements OnInit {
  readalongs: Readalong[] = []

  constructor(private readalongServices: ReadalongService, public authService: AuthService) {}

  ngOnInit(): void {
    this.readalongServices.getBooks().subscribe(() => {
      this.readalongServices.getReadalongs().subscribe((readalongs: Array<Readalong>) => {
        this.readalongs = readalongs
        console.log(this.readalongs)
      })
    })
  }
}
