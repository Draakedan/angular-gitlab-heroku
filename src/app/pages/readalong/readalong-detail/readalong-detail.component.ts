import { registerLocaleData } from '@angular/common'
import { ThisReceiver } from '@angular/compiler'
import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { AuthService } from '../../auth/auth.service'
import { User } from '../../user/user.model'
import { Readalong } from '../readalong.model'
import { ReadalongService } from '../readalong.service'

@Component({
  selector: 'app-readalong-detail',
  templateUrl: './readalong-detail.component.html',
  styles: []
})
export class ReadalongDetailComponent implements OnInit {
  readalongId: string = ''
  readalong!: Readalong
  readers: Array<User> = []
  userId: string = ''
  user!: User

  constructor(
    private route: ActivatedRoute,
    private readalongService: ReadalongService,
    public authService: AuthService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((parameters) => {
      this.readalongId = parameters.get('id') || ''
      this.readalongService.getReadalongById(this.readalongId).subscribe((readalong) => {
        this.readalong = readalong
        this.userId = readalong.userId
        this.user = this.readalongService.getOneUser(this.userId[0])
      })

      this.readalongService.getMembers(this.readalongId).subscribe((memb) => {
        memb.forEach((deelnemer) => {
          this.readers.push(deelnemer)
          console.log(deelnemer)
        })
      })
    })
  }

  removeReadalong(id: string) {
    this.readalongService.removeBook(id).subscribe(() => {
      this.router.navigate(['..'], { relativeTo: this.route })
    })
  }

  registerReadalong() {
    if (!this.isRegistered()) {
      this.readalongService.register(this.readalong._id).subscribe(() => {
        this.router.navigate(['register'], { relativeTo: this.route })
      })
    }
  }

  unregisterReadalong() {
    if (this.isRegistered()) {
      this.readalongService.unregister(this.readalong._id).subscribe(() => {
        this.router.navigate(['unregister'], { relativeTo: this.route })
      })
    }
  }

  public isRegistered(): boolean {
    var registered = false
    this.readers.forEach((user) => {
      if (user.user_id == this.authService.currentUserValue.user_id) {
        registered = true
      }
    })
    if (this.readalong.userId == this.authService.currentUserValue.user_id) registered = true
    return registered
  }

  public isUserRegisterd(id: string): boolean {
    var registered = false

    if (id == this.authService.currentUserValue.user_id) registered = this.isRegistered()

    return registered
  }
}
