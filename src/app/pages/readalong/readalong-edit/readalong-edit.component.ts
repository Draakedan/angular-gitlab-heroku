import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, ParamMap, Router } from '@angular/router'
import { of, switchMap, tap } from 'rxjs'
import { Book } from '../../book/book.model'
import { User } from '../../user/user.model'
import { Readalong, ReadalongType } from '../readalong.model'
import { ReadalongService } from '../readalong.service'
import { BookService } from '../../book/book.service'

@Component({
  selector: 'app-readalong-edit',
  templateUrl: './readalong-edit.component.html',
  styles: []
})
export class ReadalongEditComponent implements OnInit {
  bookId: string | null = null
  readalong: Readalong = new Readalong()
  books: Book[] = []
  subscription: any
  enumKeys: string[] = []

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private readalongService: ReadalongService,
    private bookService: BookService
  ) {
    this.enumKeys = Object.keys(ReadalongType)
  }

  ngOnInit(): void {
    this.bookService.getBooks().subscribe((books) => {
      this.books = books
    })
    this.subscription = this.route.paramMap
      .pipe(
        tap(console.log),
        switchMap((params: ParamMap) => {
          if (!params.get('id')) {
            return of({
              naam: '',
              _id: '',
              startDatum: new Date(),
              eindDatum: new Date(),
              readalongType: ReadalongType.open,
              minimumDeelnemers: 0,
              boek: new Book(),
              boekId: '',
              deelnemers: [new User()],
              voldoendeDeelnemers: false,
              userId: ''
            })
          } else {
            return this.readalongService.getReadalongById(params.get('id') || '')
          }
        })
      )
      .subscribe((readalong) => {
        this.readalong = readalong
      })
  }

  onSubmit(): void {
    this.readalong.boek = this.bookService.getOneBook(this.readalong.boekId)
    if (this.readalong._id) {
      this.readalongService.editReadalong(this.readalong).subscribe(() => {
        this.router.navigate(['..'], { relativeTo: this.route })
      })
    } else {
      this.readalongService.addReadalong(this.readalong).subscribe(() => {
        this.router.navigate(['..'], { relativeTo: this.route })
      })
    }
  }
}
