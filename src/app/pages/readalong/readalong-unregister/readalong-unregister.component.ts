import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'

@Component({
  selector: 'app-readalong-unregister',
  templateUrl: './readalong-unregister.component.html',
  styleUrls: ['./readalong-unregister.component.css']
})
export class ReadalongUnregisterComponent implements OnInit {
  constructor(private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {}

  return() {
    this.router.navigate(['..'], { relativeTo: this.route })
  }
}
