import { Injectable } from '@angular/core'
import { Readalong, ReadalongType } from './readalong.model'
import { BookService } from '../book/book.service'
import { UserService } from '../user/user.service'
import { Book } from '../book/book.model'
import { User, UserRole } from '../user/user.model'
import { environment } from 'src/environments/environment'
import { HttpClient } from '@angular/common/http'
import { AuthService } from '../auth/auth.service'
import { catchError, map, Observable, of } from 'rxjs'
import { read } from 'fs'

@Injectable({
  providedIn: 'root'
})
export class ReadalongService {
  users: User[] = this.getUsers()
  private readalongs: Readalong[] = []
  book!: Book

  constructor(
    private bookService: BookService,
    private userService: UserService,
    private http: HttpClient,
    private authService: AuthService
  ) {}

  getUsers(): Array<User> {
    let returnUsers: Array<User> = []
    this.userService.getUsers().subscribe((users) => {
      returnUsers = users
    })
    return returnUsers
  }

  getBooks(): Observable<any> {
    return this.bookService.getBooks()
  }

  getReadalongs(): Observable<Array<Readalong>> {
    this.readalongs = []
    return this.http.get(`${environment.apiUrl}readalongs`, { headers: this.authService.getToken() }).pipe(
      map((response: any) => {
        console.log(response)
        response.forEach((readalong: Readalong) => {
          readalong.boek = this.bookService.getOneBook(readalong.boekId)
          this.readalongs.push(readalong)
        })
        return this.readalongs
      }),
      catchError((error: any) => {
        console.log('error:', error)
        console.log('error.message:', error.message)
        console.log('error.error.message:', error.error.message)
        return undefined!
      })
    )
  }

  getReadalongById(id: string): Observable<Readalong> {
    return this.http
      .get(`${environment.apiUrl}readalongs/${id}`, { headers: this.authService.getToken() })
      .pipe(
        map((response: any) => {
          let readalong = { ...response } as Readalong
          readalong.boek = this.bookService.getOneBook(readalong.boekId)
          return readalong
        }),
        catchError((error: any) => {
          console.log('error:', error)
          console.log('error.message:', error.message)
          console.log('error.error.message:', error.error.message)
          return undefined!
        })
      )
  }

  getAllReadalongsOfList(ids: Array<String>): Array<Readalong> {
    let readalongList: Array<Readalong> = []
    ids.forEach((id) => {
      readalongList.push(this.readalongs.filter((readalong) => readalong._id === id)[0])
    })
    return readalongList
  }

  addReadalong(readalong: Readalong): Observable<Readalong> {
    this.readalongs.push(readalong)
    return this.http
      .post(
        `${environment.apiUrl}readalongs/${readalong._id}`,
        { ...readalong, id: this.authService.currentUserValue.user_id },
        { headers: this.authService.getToken() }
      )
      .pipe(
        map((response: any) => {
          console.log(response)
          let readalong = { ...response } as Readalong
          return readalong
        }),
        catchError((error: any) => {
          console.log('error:', error)
          console.log('error.message:', error.message)
          console.log('error.error.message:', error.error.message)
          return undefined!
        })
      )
  }

  editReadalong(book: Readalong): Observable<any> {
    return this.http
      .put(
        `${environment.apiUrl}readalongs/${book._id}`,
        { ...book },
        { headers: this.authService.getToken() }
      )
      .pipe(
        map((response: any) => {
          let book = { ...response } as Readalong
          return book
        }),
        catchError((error: any) => {
          console.log('error:', error)
          console.log('error.message:', error.message)
          console.log('error.error.message:', error.error.message)
          return undefined!
        })
      )
  }

  removeBook(id: string): Observable<any> {
    return this.http
      .post(
        `${environment.apiUrl}readalongs/${id}`,
        { id: this.getOneReadalong(id).userId[0] },
        { headers: this.authService.getToken() }
      )
      .pipe(
        map((response: any) => {
          let book = { ...response } as Readalong
          this.getReadalongs()
          return book
        }),
        catchError((error: any) => {
          console.log('error:', error)
          console.log('error.message:', error.message)
          console.log('error.error.message:', error.error.message)
          return undefined!
        })
      )
  }

  getMembers(id: string): Observable<Array<User>> {
    console.log('called')
    let users: Array<User> = []
    return this.http
      .get(`${environment.apiUrl}readalongs/${id}/members`, { headers: this.authService.getToken() })
      .pipe(
        map((response: any) => {
          let list = { ...response } as Array<Array<User>>
          console.log(response)
          console.log(list[0])
          users = list[0]
          return users
        }),
        catchError((error: any) => {
          console.log('error:', error)
          console.log('error.message:', error.message)
          console.log('error.error.message:', error.error.message)
          return undefined!
        })
      )
  }

  getOneReadalong(id: string): Readalong {
    console.log('red', this.readalongs)
    console.log(id)
    const readalong = this.readalongs.filter((read) => read._id === id)[0]
    console.log(readalong)
    return readalong
  }

  register(id: string): Observable<any> {
    console.log(id)
    return this.http
      .post(
        `${environment.apiUrl}readalongs/${id}/members/register`,
        { id: this.authService.currentUserValue.user_id },
        { headers: this.authService.getToken() }
      )
      .pipe(
        map((response: any) => {
          console.log(response)
          return response
        }),
        catchError((error: any) => {
          console.log('error:', error)
          console.log('error.message:', error.message)
          console.log('error.error.message:', error.error.message)
          return undefined!
        })
      )
  }

  unregister(id: string): Observable<any> {
    console.log(id)
    return this.http
      .put(
        `${environment.apiUrl}readalongs/${id}/members/unregister`,
        { id: this.authService.currentUserValue.user_id },
        { headers: this.authService.getToken() }
      )
      .pipe(
        map((response: any) => {
          console.log(response)
          return response
        }),
        catchError((error: any) => {
          console.log('error:', error)
          console.log('error.message:', error.message)
          console.log('error.error.message:', error.error.message)
          return undefined!
        })
      )
  }

  getOneUser(id: string): User {
    return this.userService.getOneUser(id)
  }
}
