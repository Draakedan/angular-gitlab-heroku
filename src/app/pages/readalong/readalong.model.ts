import { Book } from '../book/book.model'
import { User } from '../user/user.model'

export enum ReadalongType {
  open = 'open',
  closed = 'closed',
  membersOnly = 'membersOnly'
}

export class Readalong {
  naam: string = ''
  _id: string = ''
  startDatum: Date = new Date()
  eindDatum: Date = new Date()
  readalongType: ReadalongType = ReadalongType.open
  minimumDeelnemers: number = 0
  boek: Book = new Book()
  boekId: string = ''
  deelnemers: Array<User> = [new User()]
  voldoendeDeelnemers: boolean = false
  userId: string = 'i'
}
