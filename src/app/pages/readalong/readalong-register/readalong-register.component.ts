import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'

@Component({
  selector: 'app-readalong-register',
  templateUrl: './readalong-register.component.html',
  styleUrls: ['./readalong-register.component.css']
})
export class ReadalongRegisterComponent implements OnInit {
  constructor(private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {}

  return() {
    this.router.navigate(['..'], { relativeTo: this.route })
  }
}
