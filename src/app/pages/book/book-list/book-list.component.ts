import { Component, OnInit } from '@angular/core'
import { AuthService } from '../../auth/auth.service'
import { Book } from '../book.model'
import { BookService } from '../book.service'

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styles: []
})
export class BookListComponent implements OnInit {
  books: Array<Book> = []

  constructor(private bookService: BookService, public authService: AuthService) {}

  ngOnInit(): void {
    console.log('attempting to fetch book')
    this.bookService.getBooks().subscribe((books: Array<Book>) => {
      this.books = books
    })
  }
}
