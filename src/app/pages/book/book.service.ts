import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { catchError, map, Observable, of, retry } from 'rxjs'
import { environment } from 'src/environments/environment'
import { AuthService } from '../auth/auth.service'
import { User } from '../user/user.model'
import { UserService } from '../user/user.service'
import { Book, Genres } from './book.model'

@Injectable({
  providedIn: 'root'
})
export class BookService {
  books: Array<Book> = []

  constructor(private http: HttpClient, private authService: AuthService, private userService: UserService) {}

  getBooks(): Observable<Array<Book>> {
    this.books = []
    return this.http.get(`${environment.apiUrl}books`, { headers: this.authService.getToken() }).pipe(
      map((response: any) => {
        response.forEach((book: Book) => {
          this.books.push(book)
        })
        return this.books
      }),
      catchError((error: any) => {
        console.log('error:', error)
        console.log('error.message:', error.message)
        console.log('error.error.message:', error.error.message)
        return undefined!
      })
    )
  }

  getBookById(id: string): Observable<Book> {
    return this.http.get(`${environment.apiUrl}books/${id}`, { headers: this.authService.getToken() }).pipe(
      map((response: any) => {
        let books = { ...response } as Book
        return books
      }),
      catchError((error: any) => {
        console.log('error:', error)
        console.log('error.message:', error.message)
        console.log('error.error.message:', error.error.message)
        return undefined!
      })
    )
  }

  addBook(book: Book): Observable<Book> {
    console.log("add book called")

    return this.http.post(`${environment.apiUrl}books`, { ...book , id: this.authService.currentUserValue.user_id}, { headers: this.authService.getToken() }).pipe(
      map((response: any) => {
        console.log("test post map")
        let book = { ...response } as Book
        this.books.push(book)
        console.log(book.userId)
        return book
      }),
      catchError((error: any) => {
        console.log('error:', error)
        console.log('error.message:', error.message)
        console.log('error.error.message:', error.error.message)
        return undefined!
      })
    )
  }

  editBook(book: Book): Observable<any> {
    return this.http
      .put(`${environment.apiUrl}books/${book._id}`, { ...book }, { headers: this.authService.getToken() })
      .pipe(
        map((response: any) => {
          let book = { ...response } as Book
          console.log(book._id)
          return book
        }),
        catchError((error: any) => {
          console.log('error:', error)
          console.log('error.message:', error.message)
          console.log('error.error.message:', error.error.message)
          return undefined!
        })
      )
  }

  removeBook(id: string): Observable<any> {
    console.log(this.removeBook)
    return this.http.post(`${environment.apiUrl}books/${id}`, {id: this.getOneBook(id).userId[0]}, { headers: this.authService.getToken() }).pipe(
      map((response: any) => {
        let book = { ...response } as Book
        this.getBooks()
        return book
      }),
      catchError((error: any) => {
        console.log('error:', error)
        console.log('error.message:', error.message)
        console.log('error.error.message:', error.error.message)
        return undefined!
      })
    )
  }

  getOneBook(id: string): Book {
    let res = this.books.filter((book) => book._id === id)[0]
    console.log(res.userId);
    return res;
  }

  getBooksFromList(ids: Array<string>): Array<Book> {
    let returnBooks: Array<Book> = []
    ids.forEach((id) => {
      returnBooks.push(this.books.filter((book) => book._id === id)[0])
    })
    return returnBooks
  }

  getUser(id: string): Observable<User> {
    return this.userService!.getUserById(id);
  }
}
