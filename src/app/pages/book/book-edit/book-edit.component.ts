import { Route } from '@angular/compiler/src/core'
import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, ParamMap, Router } from '@angular/router'
import { of, switchMap, tap } from 'rxjs'
import { Book, Genres } from '../book.model'
import { BookService } from '../book.service'

@Component({
  selector: 'app-book-edit',
  templateUrl: './book-edit.component.html',
  styles: []
})
export class BookEditComponent implements OnInit {
  userId: string | null = null
  book: Book = new Book()
  subscription: any
  genreEnumKeys: string[] = []

  constructor(private route: ActivatedRoute, private router: Router, private bookService: BookService) {
    this.genreEnumKeys = Object.keys(Genres)
  }

  ngOnInit(): void {
    this.subscription = this.route.paramMap
      .pipe(
        tap(console.log),
        switchMap((params: ParamMap) => {
          if (!params.get('id')) {
            return of({
              titel: '',
              _id: '',
              autheur: '',
              isbn: 0,
              eersteUitgave: new Date(),
              paginas: 0,
              genre: Genres.fantasy,
              userId: ''
            })
          } else {
            return this.bookService.getBookById(params.get('id') || '')
          }
        })
      )
      .subscribe((book) => {
        this.book = book
      })
  }

  onSubmit(): void {
    if (this.book._id) {
      this.bookService.editBook(this.book).subscribe(() => {
        this.router.navigate(['..'], { relativeTo: this.route })
      })
    } else {
      this.bookService.addBook(this.book).subscribe(() => {
        this.router.navigate(['..'], { relativeTo: this.route })
      })
    }
  }
}
