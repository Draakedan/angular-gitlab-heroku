import { Component, Input, Directive, HostListener } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms'
import { ActivatedRoute, convertToParamMap, Router } from '@angular/router'
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs'
import { AuthService } from '../../auth/auth.service';
import { Book, Genres } from '../book.model'
import { BookService } from '../book.service'
import { BookEditComponent } from './book-edit.component'

@Directive({
  selector: '[routerLink]',
})
export class RouterLinkStubDirective {
  @Input('routerLink') linkParams: any;
  navigatedTo: any = null;

  @HostListener('click')
  onClick(): void {
    this.navigatedTo = this.linkParams;
  }
}

const expectedBook: Book = {
  titel: 'Name of the Wind',
  _id: '0',
  autheur: 'Patrick Rothfuss',
  isbn: 978057501406,
  eersteUitgave: new Date(2007, 3, 27),
  paginas: 662,
  genre: Genres.fantasy,
  userId: '0'
}

describe('BookEditComponent', () => {
  let component: BookEditComponent
  let fixture: ComponentFixture<BookEditComponent>

  let bookServiceSpy: any;
  let routerSpy

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BookEditComponent]
    }).compileComponents()
  })

  beforeEach(async() => {
    bookServiceSpy = jasmine.createSpyObj('BookService', ['getBookById', 'editBook'])
    routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl'])

    await TestBed.configureTestingModule({
      declarations: [BookEditComponent,
         RouterLinkStubDirective],
      imports: [FormsModule],
      providers: [
        { provide: Router, useValue: routerSpy },
        { provide: BookService, useValue: bookServiceSpy },
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(
              convertToParamMap({
                id: '0'
              })
            )
          }
        }
      ]
    }).compileComponents()
  })
  
  beforeEach(() => {

    fixture = TestBed.createComponent(BookEditComponent)
    component = fixture.componentInstance
  })

  afterEach(() => {
    fixture.destroy()
  })

  it('should create', (done) => {
    bookServiceSpy.getBookById.and.returnValue(of(expectedBook))

    fixture.detectChanges()
    expect(component).toBeTruthy()
    expect(component.book).toEqual(expectedBook)
    done()
  })

  it('should edit', (done) => {
    bookServiceSpy.editBook.and.returnValue(of(expectedBook))

    fixture.detectChanges()
    expect(component).toBeTruthy()
    done()
  })
})
