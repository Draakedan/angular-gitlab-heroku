import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { Book } from '../book.model'
import { BookService } from '../book.service'
import { AuthService } from '../../auth/auth.service'
import { User } from '../../user/user.model'

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styles: []
})
export class BookDetailComponent implements OnInit {
  bookId: string = ''
  book!: Book
  userId: string = ''
  user!: User;

  constructor(
    private route: ActivatedRoute,
    private bookService: BookService,
    public authService: AuthService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((parameters) => {
      this.bookId = parameters.get('id') || ''
      this.bookService.getBookById(this.bookId).subscribe((book: Book) => {
        this.book = book
        this.userId = book.userId
        this.bookService.getUser(this.userId).subscribe((user) => {this.user = user})
      })
    })
  }

  removeBook(): void {
    this.bookService.removeBook(this.book._id).subscribe(() => {
      this.router.navigate(['..'], { relativeTo: this.route })
    })
  }
}
