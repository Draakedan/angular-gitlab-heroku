import { Directive, HostListener, Input } from '@angular/core'
import { ComponentFixture, TestBed } from '@angular/core/testing'
import { Router, ActivatedRoute, convertToParamMap } from '@angular/router'
import { RouterTestingModule } from '@angular/router/testing'
import { of } from 'rxjs'
import { AuthService } from '../../auth/auth.service'
import { Book, Genres } from '../book.model'
import { BookService } from '../book.service'

import { BookDetailComponent } from './book-detail.component'

@Directive({
  selector: '[routerLink]'
})
export class RouterLinkStubDirective {
  @Input('routerLink') linkParams: any
  navigatedTo: any = null

  @HostListener('click')
  onClick(): void {
    this.navigatedTo = this.linkParams
  }
}

const expectedBook: Book = {
  titel: 'Name of the Wind',
  _id: '0',
  autheur: 'Patrick Rothfuss',
  isbn: 978057501406,
  eersteUitgave: new Date(2007, 3, 27),
  paginas: 662,
  genre: Genres.fantasy,
  userId: '0'
}

describe('BookDetailComponent', () => {
  let component: BookDetailComponent
  let fixture: ComponentFixture<BookDetailComponent>
  let bookServiceSpy: any
  let routerSpy
  let authServiceSpy: any

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BookDetailComponent]
    }).compileComponents()
  })

  beforeEach(async () => {
    bookServiceSpy = jasmine.createSpyObj('BookService', ['getBookById'])
    routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl'])
    authServiceSpy = jasmine.createSpyObj('AuthService', ['userMayEdit'])
    await TestBed.configureTestingModule({
      declarations: [BookDetailComponent, RouterLinkStubDirective],
      imports: [],
      providers: [
        { provide: BookService, useValue: bookServiceSpy },
        { provide: AuthService, useValue: authServiceSpy },
        { provide: Router, useValue: routerSpy },
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(
              convertToParamMap({
                id: '0'
              })
            )
          }
        }
      ]
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(BookDetailComponent)
    component = fixture.componentInstance
  })

  afterEach(() => {
    fixture.destroy()
  })

  it('should create', (done) => {
    bookServiceSpy.getBookById.and.returnValue(of(expectedBook))

    fixture.detectChanges()
    expect(component).toBeTruthy()
    expect(component.book).toEqual(expectedBook)
    done()
  })
})
