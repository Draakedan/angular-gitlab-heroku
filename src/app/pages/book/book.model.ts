export enum Genres {
  fantasy = 'fantasy',
  scifi = 'scy-fi',
  nonfinction = 'non-fiction',
  horror = 'horror',
  romance = 'romance',
  contemperary = 'contemperary',
  mystery = 'mystery',
  gothic = 'gothic',
  victorean = 'victorean'
}

export class Book {
  titel: string = ''
  _id: string = ''
  autheur: string = ''
  isbn: number = 0
  eersteUitgave: Date = new Date()
  paginas: number = 0
  genre: Genres = Genres.fantasy
  userId: string = ''
}
