import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { DashboardComponent } from './core/dashboard/dashboard.component'
import { LayoutComponent } from './core/layout/layout.component'
import { AboutComponent } from './pages/about/about.component'
import { UserDetailComponent } from './pages/user/user-detail/user-detail.component'
import { UserEditComponent } from './pages/user/user-edit/user-edit.component'
import { UserListComponent } from './pages/user/user-list/user-list.component'
import { BookDetailComponent } from './pages/book/book-detail/book-detail.component'
import { BookEditComponent } from './pages/book/book-edit/book-edit.component'
import { BookListComponent } from './pages/book/book-list/book-list.component'
import { ReadalongDetailComponent } from './pages/readalong/readalong-detail/readalong-detail.component'
import { ReadalongEditComponent } from './pages/readalong/readalong-edit/readalong-edit.component'
import { ReadalongListComponent } from './pages/readalong/readalong-list/readalong-list.component'
import { BookclubDetailComponent } from './pages/bookclub/bookclub-detail/bookclub-detail.component'
import { BookclubEditComponent } from './pages/bookclub/bookclub-edit/bookclub-edit.component'
import { BookclubListComponent } from './pages/bookclub/bookclub-list/bookclub-list.component'
import { AuthGuard } from './login-guard.guard'
import { LoginComponent } from './pages/auth/login/login.component'
import { ReadalongRegisterComponent } from './pages/readalong/readalong-register/readalong-register.component'
import { ReadalongUnregisterComponent } from './pages/readalong/readalong-unregister/readalong-unregister.component'
import { BookclubRegisterMemberComponent } from './pages/bookclub/bookclub-register-member/bookclub-register-member.component'
import { BookclubUnregisterMemberComponent } from './pages/bookclub/bookclub-unregister-member/bookclub-unregister-member.component'
import { BookclubUnregisterBookComponent } from './pages/bookclub/bookclub-unregister-book/bookclub-unregister-book.component'
import { BookclubRegisterBookComponent } from './pages/bookclub/bookclub-register-book/bookclub-register-book.component'
import { BookclubRegisterReadalongComponent } from './pages/bookclub/bookclub-register-readalong/bookclub-register-readalong.component'
import { BookclubUnregisterReadalongComponent } from './pages/bookclub/bookclub-unregister-readalong/bookclub-unregister-readalong.component'

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'login' },
      {
        path: 'dashboard',
        pathMatch: 'full',
        component: DashboardComponent
      },
      { path: 'login', component: LoginComponent },
      {
        path: 'users',
        canActivate: [AuthGuard],
        pathMatch: 'full',
        component: UserListComponent,
        children: [
          { path: 'new', pathMatch: 'full', canActivate: [AuthGuard], component: UserEditComponent },
          { path: ':id', pathMatch: 'full', canActivate: [AuthGuard], component: UserDetailComponent },
          { path: ':id/edit', pathMatch: 'full', canActivate: [AuthGuard], component: UserEditComponent }
        ]
      },
      { path: 'users/new', pathMatch: 'full', component: UserEditComponent },
      { path: 'users/:id', pathMatch: 'full', canActivate: [AuthGuard], component: UserDetailComponent },
      { path: 'users/:id/edit', pathMatch: 'full', canActivate: [AuthGuard], component: UserEditComponent },
      {
        path: 'books',
        pathMatch: 'full',
        canActivate: [AuthGuard],
        component: BookListComponent,
        children: [
          { path: 'new', pathMatch: 'full', canActivate: [AuthGuard], component: BookEditComponent },
          { path: ':id', pathMatch: 'full', canActivate: [AuthGuard], component: BookDetailComponent },
          {
            path: ':id/edit',
            pathMatch: 'full',
            canActivate: [AuthGuard],
            component: BookEditComponent
          }
        ]
      },
      { path: 'books/new', pathMatch: 'full', canActivate: [AuthGuard], component: BookEditComponent },
      { path: 'books/:id', pathMatch: 'full', canActivate: [AuthGuard], component: BookDetailComponent },
      {
        path: 'books/:id/edit',
        pathMatch: 'full',
        canActivate: [AuthGuard],
        component: BookEditComponent
      },
      {
        path: 'readalongs',
        pathMatch: 'full',
        canActivate: [AuthGuard],
        component: ReadalongListComponent,
        children: [
          { path: 'new', pathMatch: 'full', canActivate: [AuthGuard], component: ReadalongEditComponent },
          { path: ':id', pathMatch: 'full', canActivate: [AuthGuard], component: ReadalongDetailComponent },
          {
            path: ':id/register',
            pathMatch: 'full',
            canActivate: [AuthGuard],
            component: ReadalongRegisterComponent
          },
          {
            path: ':id/unregister',
            pathMatch: 'full',
            canActivate: [AuthGuard],
            component: ReadalongUnregisterComponent
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            canActivate: [AuthGuard],
            component: ReadalongEditComponent
          }
        ]
      },
      {
        path: 'readalongs/new',
        pathMatch: 'full',
        canActivate: [AuthGuard],
        component: ReadalongEditComponent
      },
      {
        path: 'readalongs/:id',
        pathMatch: 'full',
        canActivate: [AuthGuard],
        component: ReadalongDetailComponent
      },
      {
        path: 'readalongs/:id/edit',
        pathMatch: 'full',
        canActivate: [AuthGuard],
        component: ReadalongEditComponent
      },
      {
        path: 'readalongs/:id/register',
        pathMatch: 'full',
        canActivate: [AuthGuard],
        component: ReadalongRegisterComponent
      },
      {
        path: 'readalongs/:id/unregister',
        pathMatch: 'full',
        canActivate: [AuthGuard],
        component: ReadalongUnregisterComponent
      },
      {
        path: 'bookclubs',
        pathMatch: 'full',
        component: BookclubListComponent,
        children: [
          { path: 'new', pathMatch: 'full', canActivate: [AuthGuard], component: BookclubEditComponent },
          { path: ':id', pathMatch: 'full', canActivate: [AuthGuard], component: BookclubDetailComponent },
          {
            path: ':id/register',
            pathMatch: 'full',
            canActivate: [AuthGuard],
            component: BookclubRegisterMemberComponent
          },
          {
            path: ':id/unregister',
            pathMatch: 'full',
            canActivate: [AuthGuard],
            component: BookclubUnregisterMemberComponent
          },
          {
            path: ':id/registerbook',
            pathMatch: 'full',
            canActivate: [AuthGuard],
            component: BookclubRegisterBookComponent
          },
          {
            path: ':id/unregisterbook',
            pathMatch: 'full',
            canActivate: [AuthGuard],
            component: BookclubUnregisterBookComponent
          },
          {
            path: ':id/registerreadalong',
            pathMatch: 'full',
            canActivate: [AuthGuard],
            component: BookclubRegisterReadalongComponent
          },
          {
            path: ':id/unregisterreadalong',
            pathMatch: 'full',
            canActivate: [AuthGuard],
            component: BookclubUnregisterReadalongComponent
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            canActivate: [AuthGuard],
            component: BookclubEditComponent
          }
        ]
      },
      {
        path: 'bookclubs/new',
        pathMatch: 'full',
        canActivate: [AuthGuard],
        component: BookclubEditComponent
      },
      {
        path: 'bookclubs/:id',
        pathMatch: 'full',
        canActivate: [AuthGuard],
        component: BookclubDetailComponent
      },
      {
        path: 'bookclubs/:id/register',
        pathMatch: 'full',
        canActivate: [AuthGuard],
        component: BookclubRegisterMemberComponent
      },
      {
        path: 'bookclubs/:id/unregister',
        pathMatch: 'full',
        canActivate: [AuthGuard],
        component: BookclubUnregisterMemberComponent
      },
      {
        path: 'bookclubs/:id/registerbook',
        pathMatch: 'full',
        canActivate: [AuthGuard],
        component: BookclubRegisterBookComponent
      },
      {
        path: 'bookclubs/:id/unregisterbook',
        pathMatch: 'full',
        canActivate: [AuthGuard],
        component: BookclubUnregisterBookComponent
      },
      {
        path: 'bookclubs/:id/registerreadalong',
        pathMatch: 'full',
        canActivate: [AuthGuard],
        component: BookclubRegisterReadalongComponent
      },
      {
        path: 'bookclubs/:id/unregisterreadalong',
        pathMatch: 'full',
        canActivate: [AuthGuard],
        component: BookclubUnregisterReadalongComponent
      },
      {
        path: 'bookclubs/:id/edit',
        pathMatch: 'full',
        canActivate: [AuthGuard],
        component: BookclubEditComponent
      },
      { path: 'about', component: AboutComponent }
    ]
  },
  { path: '**', redirectTo: '/' }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
