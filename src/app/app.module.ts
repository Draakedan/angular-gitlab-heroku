import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { RouterModule } from '@angular/router'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { DashboardComponent } from './core/dashboard/dashboard.component'
import { NavbarComponent } from './core/navbar/navbar.component'
import { LayoutComponent } from './core/layout/layout.component'
import { FooterComponent } from './core/footer/footer.component'
import { AboutComponent } from './pages/about/about.component'
import { UsecaseComponent } from './pages/about/usecases/usecase.component'
import { UserListComponent } from './pages/user/user-list/user-list.component'
import { UserDetailComponent } from './pages/user/user-detail/user-detail.component'
import { UserEditComponent } from './pages/user/user-edit/user-edit.component'
import { BookListComponent } from './pages/book/book-list/book-list.component'
import { BookDetailComponent } from './pages/book/book-detail/book-detail.component'
import { BookEditComponent } from './pages/book/book-edit/book-edit.component'
import { ReadalongListComponent } from './pages/readalong/readalong-list/readalong-list.component'
import { ReadalongDetailComponent } from './pages/readalong/readalong-detail/readalong-detail.component'
import { ReadalongEditComponent } from './pages/readalong/readalong-edit/readalong-edit.component'
import { BookclubListComponent } from './pages/bookclub/bookclub-list/bookclub-list.component'
import { BookclubEditComponent } from './pages/bookclub/bookclub-edit/bookclub-edit.component'
import { BookclubDetailComponent } from './pages/bookclub/bookclub-detail/bookclub-detail.component'
import { LoginComponent } from './pages/auth/login/login.component';
import { NavloginComponent } from './core/navbar/navlogin/navlogin.component';
import { ReadalongRegisterComponent } from './pages/readalong/readalong-register/readalong-register.component';
import { ReadalongUnregisterComponent } from './pages/readalong/readalong-unregister/readalong-unregister.component';
import { BookclubRegisterMemberComponent } from './pages/bookclub/bookclub-register-member/bookclub-register-member.component';
import { BookclubUnregisterMemberComponent } from './pages/bookclub/bookclub-unregister-member/bookclub-unregister-member.component';
import { BookclubRegisterBookComponent } from './pages/bookclub/bookclub-register-book/bookclub-register-book.component';
import { BookclubRegisterReadalongComponent } from './pages/bookclub/bookclub-register-readalong/bookclub-register-readalong.component';
import { BookclubUnregisterBookComponent } from './pages/bookclub/bookclub-unregister-book/bookclub-unregister-book.component';
import { BookclubUnregisterReadalongComponent } from './pages/bookclub/bookclub-unregister-readalong/bookclub-unregister-readalong.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LayoutComponent,
    DashboardComponent,
    FooterComponent,
    AboutComponent,
    UsecaseComponent,
    UserListComponent,
    UserDetailComponent,
    UserEditComponent,
    BookListComponent,
    BookDetailComponent,
    BookEditComponent,
    ReadalongListComponent,
    ReadalongDetailComponent,
    ReadalongEditComponent,
    BookclubListComponent,
    BookclubEditComponent,
    BookclubDetailComponent,
    LoginComponent,
    NavloginComponent,
    ReadalongRegisterComponent,
    ReadalongUnregisterComponent,
    BookclubRegisterMemberComponent,
    BookclubUnregisterMemberComponent,
    BookclubRegisterBookComponent,
    BookclubRegisterReadalongComponent,
    BookclubUnregisterBookComponent,
    BookclubUnregisterReadalongComponent,
  ],
  imports: [BrowserModule, RouterModule, NgbModule, AppRoutingModule, FormsModule, HttpClientModule, ReactiveFormsModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
