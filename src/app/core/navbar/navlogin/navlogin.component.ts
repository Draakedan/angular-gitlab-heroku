import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/pages/user/user.model';
import { AuthService } from '../../../pages/auth/auth.service';

@Component({
  selector: 'app-navlogin',
  templateUrl: './navlogin.component.html',
  styleUrls: ['./navlogin.component.css']
})
export class NavloginComponent implements OnInit, OnChanges {
  public user: User | undefined;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.user = this.authService.currentUserValue;
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.user = this.authService.currentUserValue;
    
  }

  logout(): void {
    this.authService.logout();
    this.router.navigate(['/login'])
  }

}
