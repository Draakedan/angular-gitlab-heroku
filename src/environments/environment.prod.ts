const packagejson = require('../../package.json')

export const environment = {
  production: true,

  // Fill in your own online server API url here
  apiUrl: 'https://client-side-api-nosql-2122681.herokuapp.com/api/',

  version: packagejson.version
}
